// TEAM

// First team member name : Lyudmila Sandomirskaya

// Second team member name : David Shulman

//The name of the input file is input.txt
//the name of the output file is stacktrace.txt
When opening stacktrace.txt please make sure that tab size=8 (which is default for notepad, 
but not for notepad++), otherwise our formatting will be off.

//extract the zipped folder into your working directory
//change the working directory to the folder containing driver.c and the rest of the files
//place your desired input file in the same folder that you extracted the zipped folder into
//rename the input file "input.txt" if it does not already have that name

//run the following command in terminal to compile the project:

	gcc lexer.c latest_parser.c vm.c  driver.c -o driver.exe

//to run the program, type driver.exe into the console with any of the following arguments:
	-l  Print the list of lexemes/tokens (scanner output) to the screen
	-a  Print the generated assembly code (parser/codegen output) to the screen
	-v   Print virtual machine execution trace (virtual machine output) to the screen
	
	Example: driver.exe -l
				OR
		 driver -l
	This will print to the screen the list of lexemes.

//follow any further instructions on screen for user input

//the output file "stacktrace.txt" should be filled with the correct output
//if there are any errors, they will be printed to the screen and the execution will terminate
//in the case of errors, the parser will not generate assembly code and the output file will be empty