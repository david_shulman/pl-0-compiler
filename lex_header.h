#ifndef LEX_HEADER_H_INCLUDED
#define LEX_HEADER_H_INCLUDED

#define NO_RW 15 /* number of reserved words */
#define INT_MAX 32767 /* maximum integer value */
#define IDENT_LEN 11 /* maximum number of chars for idents */
//#define nestmax 5 /* maximum depth of block nesting */
#define strmax 256 /* maximum length of strings */
#define NUM_LEN 5 /*max length of numbers */
#define RESERVED_LEN 14 /* actual number of reserved words */
#define NO_SYMBS 21 /*number of operators such as +, -, :=, etc */
#define NO_SNGL_SYMBS 13

typedef enum {
nulsym = 1, identsym = 2, numbersym = 3, plussym = 4, minussym = 5,
multsym = 6, slashsym = 7, oddsym = 8, eqsym = 9, neqsym = 10, lessym = 11, leqsym = 12,
gtrsym = 13, geqsym = 14, lparentsym = 15, rparentsym = 16, commasym = 17, semicolonsym = 18,
periodsym = 19, becomessym = 20, beginsym = 21, endsym = 22, ifsym = 23, thensym = 24,
whilesym = 25, dosym = 26, callsym = 27, constsym = 28, varsym = 29, procsym = 30, writesym = 31,
readsym = 32, elsesym = 33
} token_type;

typedef struct TokenTable
{
    token_type id;
    char lexeme[IDENT_LEN];
		int size;
} TokenTable;

typedef struct LegalSymTable
{
    token_type id;
    char sym;
} LegalSymTable;

int isDigit(char ch);
int isAlphaNum(char ch);
int isWhiteSpace(char ch);
int isOperator(char ch);
int findLength(char* array[]);
TokenTable getReservedToken(char* resString);
int isSymbol(char s);
int isReserved(char* resString);
void assignSingleLegalSymbols();
void assignSymbols();
int isLegalSymbol(char ch);
token_type getOpID(char *ch);
token_type getSymID(char ch);
char* readInput(FILE *input, FILE *output);
void initReserved();
int printLexemeTable(TokenTable *table);
int printLexemeList(TokenTable *table);
void identifyTokens(char* str);
void lexerMain(int lArg);

#endif