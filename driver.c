/* David Shulman and Lyudmila Sandomirskaya */
void error(int e);
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vm_header.h"
#include "lex_header.h"
#include "parse_header.h"


#define inputDBG 0


//instr* genTempCode();

int main(int argc, char **argv){
	int lArg=0, aArg=0, vArg=0;
	// if ((argc-1) == 0)
		// return 0;
	//check for 
	//-l : print the list of lexemes/tokens (scanner output) to the screen
	//-a : print the generated assembly code (parser/codegen output) to the screen
	//-v : print virtual machine execution trace (virtual machine output) to the screen
	//filename (no code)
	int i=0;
	//printf("Didn't read arguments yet\n");
	for(i=0; i<argc; i++){
		if(inputDBG) printf("%s\n",argv[i]);
		if(strcmp(argv[i], "-l") == 0){
			lArg = 1;
			continue;
		}
		if(strcmp(argv[i], "-a") == 0){
			aArg = 1;
			continue;
		}
		if(strcmp(argv[i], "-v") == 0){
			vArg = 1;
			continue;
		}
	}
	//printf("Read the arguments\n");
	//lArg = 0; aArg = 1; vArg = 1; // ****param override remove later****
	if(inputDBG) printf("%d %d %d\n",lArg, aArg, vArg);
	
	//initSymbolTable();
	
	char* filename = malloc(sizeof(char)*20);
	strcpy(filename,argv[argc-1]);
	int error = 0;
	if(inputDBG) printf("%s\n",filename);
	if(inputDBG) printf("Got to lexer!\n");
	lexerMain(lArg);
	if(inputDBG) printf("Got to parser\n");
	error = parserMain(aArg);
	if(inputDBG) printf("Got to vm!\n");
	if(error == 0)
	{
		printf("\n\nThis program is syntactically correct!\n");
		vmMain(vArg);
	}
	if(inputDBG) printf("Got to end!\n");
	return 0;
}
