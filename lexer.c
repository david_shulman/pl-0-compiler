#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lex_header.h"

// #define NO_RW 15 /* number of reserved words */
// #define INT_MAX 32767 /* maximum integer value */
// #define IDENT_LEN 11 /* maximum number of chars for idents */
// //#define nestmax 5 /* maximum depth of block nesting */
// #define strmax 256 /* maximum length of strings */
// #define NUM_LEN 5 /*max length of numbers */
// #define RESERVED_LEN 14 /* actual number of reserved words */
// #define NO_SYMBS 21 /*number of operators such as +, -, :=, etc */
// #define NO_SNGL_SYMBS 13

// typedef enum {
// nulsym = 1, identsym = 2, numbersym = 3, plussym = 4, minussym = 5,
// multsym = 6, slashsym = 7, oddsym = 8, eqsym = 9, neqsym = 10, lessym = 11, leqsym = 12,
// gtrsym = 13, geqsym = 14, lparentsym = 15, rparentsym = 16, commasym = 17, semicolonsym = 18,
// periodsym = 19, becomessym = 20, beginsym = 21, endsym = 22, ifsym = 23, thensym = 24,
// whilesym = 25, dosym = 26, callsym = 27, constsym = 28, varsym = 29, procsym = 30, writesym = 31,
// readsym = 32, elsesym = 33
// } token_type;

// typedef struct TokenTable
// {
    // token_type id;
    // char lexeme[IDENT_LEN];
		// int size;
// } TokenTable;

// typedef struct LegalSymTable
// {
    // token_type id;
    // char sym;
// } LegalSymTable;
int lArg;
char* inputFile = "input.txt";
TokenTable reserved[NO_RW+1];


char symbols[] = {
	'+', '-', '*', '/', '(', ')', '=', ',', '.',
	'<', '>', ';', ':',
};

char* specSym[33];
LegalSymTable legalSym[NO_SNGL_SYMBS];

int isDigit(char ch)
{
    int ascii_val=(int)ch;
    if ((int)ascii_val>47 && (int)ascii_val<58)
        return 1;
    else
        return 0;
}

int isAlphaNum(char ch)
{
    int ascii_val=(int)ch;
    //check for a digit
    //printf("\nin aplhanum for %c with acsii %d", ch,(int)ch);
    if ((int)ascii_val>47 && (int)ascii_val<58)
    {
        //printf("\n%c is alpha num", ch);
        return 1;
    }

    //check for letter
    if (((int)ascii_val>64 && (int)ascii_val<91) || ((int)ascii_val>96 && (int)ascii_val<123))
        return 1;
    else
        return 0;
}

int isWhiteSpace(char ch)
{
    if((int)ch < 33){
        return 1;
	}
    return 0;
}

int isOperator(char ch)
{
    if(ch=='+' || ch=='-' ||ch==':' || ch=='/' || ch=='*' || ch=='>' || ch=='<' || ch=='=')
        return 1;
    return 0;
}

int findLength(char* array[]){
	int i = 0;
	while(array[i] != NULL){
		//printf("Current reserved word: %s\n", reserved[i]);
		i++;
	}
	//printf("This is the length of reserved: %d\n", i);
	return i;
}

TokenTable getReservedToken(char* resString)
{
	int i=0;

	while(i < RESERVED_LEN){
		if(strcmp(resString, reserved[i].lexeme) == 0){
			return reserved[i];
		}
		i++;
	}
	//return NULL;

}

int isSymbol(char s){
	int i=0;
	if((int)s == 0){
		return 0;
	}
	while(s != symbols[i]){
		//printf("This is the current symbol: %c\n", symbols[i]);
		i++;
	}
	if(symbols[i] == s){
		//printf("This is the matching symbol: %c\n", symbols[i]);
		return 1;
	}
	return 0;
}

int isReserved(char* resString){
	int i=0;
	if(resString == NULL){
		return 0;
	}
	while(i < RESERVED_LEN){
		if(strcmp(resString, reserved[i].lexeme) == 0){
			return 1;
		}
		i++;
	}
	return 0;
}

void assignSingleLegalSymbols(){
    legalSym[0].id=nulsym;
    legalSym[0].sym = '\0';
	legalSym[1].id=plussym;
	legalSym[1].sym = '+';
	legalSym[2].id=slashsym;
	legalSym[2].sym = '/';
	legalSym[3].id = eqsym;
	legalSym[3].sym = '=';
	legalSym[4].id = minussym;
	legalSym[4].sym = '-';
	legalSym[5].id = lparentsym;
	legalSym[5].sym = '(';
	legalSym[6].id = commasym;
	legalSym[6].sym = ',';
	legalSym[7].id = lessym;
	legalSym[7].sym = '<';
	legalSym[8].id = multsym;
	legalSym[8].sym = '*';
	legalSym[9].id = rparentsym;
	legalSym[9].sym = ')';
	legalSym[10].id = periodsym;
	legalSym[10].sym = '.';
	legalSym[11].id = gtrsym;
	legalSym[11].sym = '>';
	legalSym[12].id = semicolonsym;
	legalSym[12].sym = ';';
	}

void assignSymbols()
{
  int j;
  for (j=0; j<NO_SYMBS;j++)
    specSym[j] = "";
    specSym[nulsym] = "\0";
		specSym[plussym] = "+";
		specSym[slashsym] = "/";
		specSym[eqsym] = "=";
		specSym[neqsym] = "<>";
		specSym[leqsym] = "<=";
		specSym[minussym] = "-";
		specSym[lparentsym] = "(";
		specSym[commasym] = ",";
		specSym[lessym] = "<";
		specSym[geqsym] = ">=";
		specSym[multsym] = "*";
		specSym[rparentsym] = ")";
		specSym[periodsym] = ".";
		specSym[gtrsym] = ">";
		specSym[semicolonsym] = ";";
		specSym[becomessym] = ":=";
}

int isLegalSymbol(char ch)
{
    int i=0;
    for (i=0;i<NO_SNGL_SYMBS; i++)
    {
        //printf("\nlegalSym[i].sym = %c",legalSym[i].sym);
        if (legalSym[i].sym==ch)
            return 1;
    }
    return 0;
}
token_type getOpID(char *ch)
{
    int i=0;
    for (i=0;i<findLength(specSym); i++)
    {
        //printf("\nspecSym[i] = %s",specSym[i]);
        if (strcmp(specSym[i],ch)==0)
            return i;
    }
    return 0;
}

token_type getSymID(char ch)
{
    int i=0;
    for (i=0;i<NO_SNGL_SYMBS; i++)
    {
        if (legalSym[i].sym==ch)
            return legalSym[i].id;
    }
    return i;
}
int len=0;

char* readInput(FILE *input, FILE *output){
	int i=0, inside_comment=0;
	input = fopen("input.txt","r");
	if( input == NULL )
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }
    //read input file
    while (!feof(input))
    {
      char ch1;
      fscanf(input, "%c", &ch1);
      len++;
    }
    fclose(input);
    input = fopen("input.txt","r"); // open input file again
    char* clean_str = (char*)calloc(len,sizeof(char));
    char* str = (char*)calloc(len,sizeof(char));
    while (!feof(input))
    {
      fscanf(input, "%c", &str[i]);
      i++;
    }
    int k=0, indx_clean=0;
    while(k<len-1 && str[k] != '\0')
    {
        if ((str[k]=='/') && (str[k+1]=='*'))
        {
             k+=2;
             inside_comment=1;
             while (((str[k]!='*') || (str[k+1]!='/')) && (k<len-1))
                k++;
        }

        if ((str[k]=='*') && (str[k+1]=='/'))
        {
            k+=2;
            inside_comment=0;
        }

        fprintf(output,"%c",str[k]);
        clean_str[indx_clean]=str[k];
        k++;
        indx_clean++;

        if (k>=len-1 && inside_comment==1)
        {
            printf("\nIncorrect comment closing");
            //delete everything from cleaninput.txt
            fclose(output);
            output = fopen("cleaninput.txt","w");
            fclose(input);
            fclose(output);
            return NULL;
        }

    }
    fclose(input);
    fclose(output);
	return clean_str;
}

//#define LEN len
//TokenTable token_table[LEN];
void initReserved()
{
    reserved[0].id = constsym;
    strcpy(reserved[0].lexeme,"const");
    reserved[1].id = varsym;
    strcpy(reserved[1].lexeme, "var");
    reserved[2].id = procsym;
    strcpy(reserved[2].lexeme, "procedure");
    reserved[3].id = callsym;
    strcpy(reserved[3].lexeme, "call");
    reserved[4].id = beginsym;
    strcpy(reserved[4].lexeme, "begin");
    reserved[5].id = endsym;
    strcpy(reserved[5].lexeme, "end");
    reserved[6].id = ifsym;
    strcpy(reserved[6].lexeme, "if");
    reserved[7].id = thensym;
    strcpy(reserved[7].lexeme, "then");
    reserved[8].id = elsesym;
    strcpy(reserved[8].lexeme, "else");
    reserved[9].id = whilesym;
    strcpy(reserved[9].lexeme, "while");
    reserved[10].id = dosym;
    strcpy(reserved[10].lexeme, "do");
    reserved[11].id = readsym;
    strcpy(reserved[11].lexeme, "read");
    reserved[12].id = writesym;
    strcpy(reserved[12].lexeme, "write");
    reserved[13].id = oddsym;
    strcpy(reserved[13].lexeme, "odd");
}

int printLexemeTable(TokenTable *table)
{
	FILE *lextable = fopen("lexemetable.txt","w");
	int i=0;
	fprintf(lextable,"Lexeme\tToken type");
	for (i=0; i<table[0].size; i++)
	{
		fprintf(lextable,"\n%s\t%d ", table[i].lexeme, table[i].id);
		//printf("\n%s\t%d ", table[i].lexeme, table[i].id);
	}
	fclose(lextable);
    return 0;
}

int printLexemeList(TokenTable *table)
{
	//printf("Got here?\n");
	FILE *lexlist = fopen("lexemelist.txt","w");
	int i=0;
	//printf("\nlexemelist size: %d\n",table[0].size);
	for (i=0; i<table[0].size; i++)
	{

		fprintf(lexlist,"%d ", table[i].id);
		if(lArg == 1){printf("%d ", table[i].id);}
		if (table[i].id==2 || table[i].id==3)
		{
           if ((int)table[i].lexeme!=0)
            fprintf(lexlist,"%s ", table[i].lexeme);
			if(lArg == 1){printf("%s ", table[i].lexeme);}
		}
		//printf("\n%s\t%d ", table[i].lexeme, table[i].id);
	}
	if(lArg == 1) printf("\n");
	fclose(lexlist);
    return 0;
}


void identifyTokens(char* str){
	//counters
   int i=0, t_size = 0;
   //flags
   int inside_num=0, inside_ident=0, inside_op = 0;
   char num[IDENT_LEN], ident[IDENT_LEN];
   TokenTable table[len];
   TokenTable *token_table = table;
   while (i<strlen(str))
   {
       //skip white spaces
        for (i;i < strlen(str);i++)
        {
            if(str[i]==' ' || str[i]=='\t' || str[i]=='\n' || str[i]=='\r')
                continue;
            else
                break;
        }
        //check if token is a digit
        if (isDigit(str[i])&& (i<strlen(str)))
        {
            int v=0,num_len=0;
            inside_num=1;
            do
            {
                //printf("\nascii value: %d", (int)str[i]);
                v=10*v+(int)str[i];
                num[num_len]=str[i];
                num[num_len+1]='\0';
                i++;
                num_len++;
                if(num_len>=NUM_LEN)
                {
                    printf("Number is too long");
					exit(0);
                    //return NULL;
                }
            }while (isDigit(str[i])&& (i<strlen(str)));

            if ((inside_num==1)&&(isWhiteSpace(str[i]) || isOperator(str[i])
                  || str[i]=='(' || str[i]==')' || str[i]==';' || str[i]==',' || str[i]==':' || (int)str[i]==0))
            {
                //token_table[t_size].val = num;
                token_table[t_size].id = numbersym;
                strcpy(token_table[t_size].lexeme,num);
                //printf("\ntoken id:%d lexeme: %s",token_table[t_size].id,token_table[t_size].lexeme);
                //if(isWhiteSpace(str[i]) || ((int)str[i]==0))
                t_size++;
								token_table[0].size = t_size;
                inside_num=0;
				continue;
            }

            else
                if(inside_num==1)
                {
                    printf("\nVariable does not start with letter %s",num);
					exit(0);
                    //return NULL;

                }
        }

        //skip white spaces
        for (i;i < strlen(str);i++)
        {
            if(str[i]==' ' || str[i]=='\t' || str[i]=='\n' || str[i]=='\r')
                continue;
            else
                break;
        }

        //check if token is identifier or reserved word
        if (isAlphaNum(str[i])&& (i<strlen(str)))
        {

                int ident_len=0;
                inside_ident=1;
                do
                {
                    //printf("\nascii value: %d", (int)str[i]);
                    ident[ident_len]=str[i];
                    ident[ident_len+1]='\0';
                    i++;
                    ident_len++;
                    if(ident_len>IDENT_LEN)
                    {
                        printf("Identifier name is too long");
						exit(0);
                        //return NULL;
                    }
                }while (isAlphaNum(str[i])&& (i<strlen(str)));

            //printf("\nascii value: %d", (int)str[i]);
            if ((inside_ident==1)&&(isWhiteSpace(str[i]) || isOperator(str[i]) || str[i]==';' || str[i]==','
									|| str[i]=='(' || str[i]==')' || str[i]=='.' || (int)str[i]==0))
            {
                inside_ident=0;

                if (isReserved(ident))
                {
                    TokenTable res_token = getReservedToken(ident);
                    token_table[t_size].id = res_token.id;
                    strcpy(token_table[t_size].lexeme,ident);
                    //printf("\ntoken id:%d lexeme: %s",token_table[t_size].id,token_table[t_size].lexeme);
                    t_size++;
										token_table[0].size = t_size;
                    continue;
                }
                else
                {
                    token_table[t_size].id = identsym;
                    strcpy(token_table[t_size].lexeme,ident);
                    //printf("\ntoken id:%d lexeme: %s",token_table[t_size].id,token_table[t_size].lexeme);
                    t_size++;
										token_table[0].size = t_size;
                    continue;
                }

            }
            else{
                if (inside_ident==1)
                {
					//printf("This is str[i]: %d\n", str[i]);
                    printf("\nInvalid symbols in identifier's name\n");
					exit(0);
                    //return NULL;

                }

			}
        }//end isAlphaNum

		//skip white spaces
        for (i;i < strlen(str);i++)
        {
            if(str[i]==' ' || str[i]=='\t' || str[i]=='\n' || str[i]=='\r')
                continue;
            else
                break;
        }

		if (isOperator(str[i]))
        {
            inside_op=1;
            char op[3];
            //printf("\nnext ascii value: %d", (int)str[i]);
            op[0]=str[i];
            op[1]='\0';
            i++;


            if (isOperator(str[i]))
            {
                op[1]=str[i];
                op[2]='\0';
                //if operator is found in specSym move to the next character
                if(getOpID(op)!=0)
                {
                    i++;
                }
                else
                    if (inside_op=1)
                    {
                        printf("\nIncorrect operator: %s",op);
						exit(0);
                        //return NULL;
                    }
            }
            if ((inside_op==1)&&((isWhiteSpace(str[i]) || isOperator(str[i])
                || str[i]=='(' || str[i]==')' || str[i]==';' || str[i]==',' || (int)str[i]==0) || isAlphaNum(str[i])))
            {
                token_table[t_size].id = getOpID(op);
                strcpy(token_table[t_size].lexeme,op);
                //printf("\ntoken id:%d lexeme: %s",token_table[t_size].id,token_table[t_size].lexeme);
                //if (!isAlphaNum(str[i]))
                t_size++;
								token_table[0].size = t_size;
                inside_op=0;
                continue;
            }

            else
                if (inside_op=1)
                {
                    printf("\nIncorrect operator: %s%c",op, str[i]);
					exit(0);
                    //return NULL;
                }
        }
        //skip white spaces
        for (i;i < strlen(str);i++)
        {
            if(str[i]==' ' || str[i]=='\t' || str[i]=='\n' || str[i]=='\r')
                continue;
            else
                break;
        }

        if (isLegalSymbol(str[i]))
        {
            char symb[2];
            symb[0]=str[i];
            symb[1]='\0';
            token_table[t_size].id = getSymID(str[i]);
            strcpy(token_table[t_size].lexeme,symb);
            //printf("\ntoken id:%d lexeme: %s",token_table[t_size].id,token_table[t_size].lexeme);
            t_size++;
						token_table[0].size = t_size;
            i++;
            continue;
        }
        else
        {
            printf("\nIllegal Symbol: %c", str[i]);
			exit(0);
                    //return NULL;
        }

        i++;
   }
   //printf("Hello?\n");
   printLexemeTable(token_table);
   printLexemeList(token_table);
   //free(token_table);
   //return token_table;
}

void lexerMain(int Arg){
	lArg = Arg;
   //printf("Lexer is running\n");
   FILE *input, *output /* ,*lextable, *lexlist */;
   char* inputString;
   char mystr[50];
   //TokenTable *token_table;
   input = fopen(inputFile,"r"); // open input file
   output = fopen("cleaninput.txt","w"); // open output file
   // lextable = fopen("lexemetable.txt","w");
   // lexlist = fopen("lexemelist.txt","w");
   inputString = readInput(input, output);
   // if(lArg == 1){
		// printf("\n%s\n",inputString);
   // }
   //printf("\n%s\n",mystr);
   if (inputString==NULL)
	   exit(1);
      //return 0;
   assignSingleLegalSymbols();
   assignSymbols();
   initReserved();
   //token_table=identifyTokens(inputString);
   //identifyTokens(mystr);
   // if(lArg == 1){
	   // printLexemeList(token_table);
   // }
   //free(token_table);
   //token_table = identifyTokens(inputString);
   identifyTokens(inputString);

   //free(token_table);
   //return 0;
}

// void main(){
	// lexerMain(1);
//}


