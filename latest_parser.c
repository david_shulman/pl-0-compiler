/* David Shulman and Lyudmila Sandomirskaya */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vm_header.h"
#include "lex_header.h"
#include "parse_header.h"

#define CODE_SIZE 500
#define SYMB_TABLE_SIZE 100
#define DEBUG_ON 0

typedef struct symbol{
	char* name;
	int kind; //const=1, var=2, proc=3 (-1 for empty)
	int addr;
	int level;
	int value; //only used for const
	int line;
} symbol;

void initSymbolTable();
void addSymbol(char* name, int kind, int level, int line);
int lookUp(char* name);
void updateValue(char* name, int val);
char** OP_Strings;
symbol symbolTable[SYMB_TABLE_SIZE];
int next_indx;
int delete_from_indx=0;
int varAddr;
int num_vars=0;
int lexLevel = 0;
int errorBool = 0;

char* lList = "lexemelist.txt";//"input10lex.txt"; //"lexemelist.txt";
char* sampleList = "29 2 x 17 2 y 18 21 2 y 20 3 3 18 2 x 20 2 y 4 3 56 18 22 19";
int cx=0; //assembly code index

void factor();

/*typedef enum {
	 nulsym = 1, identsym = 2, numbersym = 3, plussym = 4, minussym = 5,
	 multsym = 6, slashsym = 7, oddsym = 8, eqsym = 9, neqsym = 10, lessym = 11, leqsym = 12,
	 gtrsym = 13, geqsym = 14, lparentsym = 15, rparentsym = 16, commasym = 17, semicolonsym = 18,
	 periodsym = 19, becomessym = 20, beginsym = 21, endsym = 22, ifsym = 23, thensym = 24,
	 whilesym = 25, dosym = 26, callsym = 27, constsym = 28, varsym = 29, procsym = 30, writesym = 31,
	 readsym = 32, elsesym = 33
	 } token;  */

typedef struct lexeme {
	int type;
	token_type number;
	char variable[12];
}lexeme;

lexeme tokenList[CODE_SIZE];
int tokenIndex = 0;
int tokenList_size = 0;
symbol symbol_table[CODE_SIZE];
char const_name[12];
instruction assembly[CODE_SIZE];

char* errors[] = {
	"Error 0: \n",
	"Error 1: Used = instead of := \n",
	"Error 2: = must be followed by a number\n",
	"Error 3: Identifier must be followed by =\n",
	"Error 4: const, var, procedure must be followed by identifier\n",
	"Error 5: Semicolon or comma missing\n",
	"Error 6: Incorrect symbol after procedure declaration\n",
	"Error 7: Statement expected\n",
	"Error 8: Incorrect symbol after statement part in block\n",
	"Error 9: Period expected\n",
	"Error 10: Semicolon between statements missing\n",
	"Error 11: Undeclared identifier\n",
	"Error 12: Assignment to constant or procedure is not allowed\n",
	"Error 13: Assignment operator expected\n",
	"Error 14: call must be followed by an identifier\n",
	"Error 15: Call of a constant or variable is meaningless\n",
	"Error 16: then expected\n",
	"Error 17: Semicolon or } expected\n",
	"Error 18: do expected\n",
	"Error 19: Incorrect symbol following statement\n",
	"Error 20: Relational operator expected\n",
	"Error 21: Expression must not contain a procedure identifier\n",
	"Error 22: Right parenthesis missing\n",
	"Error 23: The preceding factor cannot begin with this symbol\n",
	"Error 24: An expression cannot begin with this symbol\n",
	"Error 25: This number is too large\n",
	"Error 26: End expected\n",
	"Error 27: Variable or constant already declared\n",
	"Error 28: Input file too long\n",
	"Error 29: Identifier expected\n",
	"Error 30: Variable expected\n"
};


// enum OPR_Type{RET = 0, NEG = 1, ADD = 2, SUB = 3, MUL = 4, DIV = 5, ODD = 6, MOD = 7, EQL = 8 , NEQ = 9, LSS = 10, LEQ = 11, GTR = 12, GEQ = 13};
// enum SIO_Type{POPPRINT = 0, READPUSH = 1, HALT = 2};
// enum ISA_Type{EMPTY, LIT, OPR, LOD, STO, CAL, INC, JMP, JPC, SIO};

//instruction assembly[CODE_SIZE];



void error(int e)
{
		if(DEBUG_ON) printf("\nindex: %d Token: %d  ", tokenIndex, tokenList[tokenIndex].number);
		if (tokenList[tokenIndex++].type ==2)
			if(DEBUG_ON) printf("name:  %s", tokenList[tokenIndex].variable);

		printf("\n");
		printf("%s", errors[e]);
		errorBool = 1;
		exit(0);

}



/*Make sure we initialize table before putting any data in it */
void   initSymbolTable()
{
	next_indx = 0;
	varAddr = 4; /*stores addr of variable on a stack
							relative to the base of current AR */
	int i = 0;

	//symbolTable = malloc(sizeof(symbol) * 500);

	for (i = 0; i < SYMB_TABLE_SIZE; i++){

		symbolTable[i].name = malloc(sizeof(char)*12);
		strcpy(symbolTable[i].name, "1");
		symbolTable[i].kind = -1; //empty
		symbolTable[i].addr = -1;
		symbolTable[i].value = 0;
		symbolTable[i].level = -7;
		symbolTable[i].line = -1;
	}
}

void addSymbol(char* name, int kind, int level, int line)
{
	if(DEBUG_ON) printf("\nInside addsymbol\n");
	//check if kind is already in table
	int i;
	for(i = 0; i < next_indx; i++)
	{
		if(DEBUG_ON) printf("\nindex: %d symbol name: %s  kind: %d level: %d",i,symbolTable[i].name, symbolTable[i].kind,symbolTable[i].level);
		if((strcmp(symbolTable[i].name, name) == 0) && (symbolTable[i].level==level))
		{
			error(27);//variable already declared
			return;
		}

	}
	if(kind == 3)
	{
		symbolTable[next_indx].line = line;
	}
	strcpy(symbolTable[next_indx].name, name);
	symbolTable[next_indx].kind = kind;
	symbolTable[next_indx].addr = getNumVars(level)+4;
	symbolTable[next_indx].level = level;
	if(DEBUG_ON)
	{
		printf("\n Symbol Table\n");
		printf("\n-------------------------\n");
		printf("\nindex: %d \nsymbol name: %s \nkind: %d \naddr: %d\n level: %d\n",i,symbolTable[i].name, symbolTable[i].kind, symbolTable[i].addr, symbolTable[i].level);
		printf("\n-------------------------\n");
	}

	next_indx++;
	if(kind == 2)
	{
		num_vars++;
		varAddr++;
	}
}

getNumVars(int level)
{
	int number_of_variables_on_level=0;
	int i;
	for(i = 0; i < next_indx; i++)
	{
		if((symbolTable[i].level == level) && (symbolTable[i].kind == 2))
		{
			number_of_variables_on_level++;

		}

	}
	return number_of_variables_on_level;
}

int lookUp(char* name){
	int i = 0;

	//for(i = 0; i < next_indx; i++)
	for(i=next_indx-1; i>=0; i--)
	{
		if(strcmp(symbolTable[i].name, name) == 0)
		{
			if ( ((symbolTable[i].kind==1) || (symbolTable[i].kind==3)) && (tokenList[tokenIndex+1].number==becomessym))
			
				error(12);
			else
				return i;
		}

	}
	error(11); //undeclared identifer
}

void deleteSymbols(int procIndex)
{
	
	next_indx = procIndex+1;
	if(DEBUG_ON)
	{
		printf("\n Next Index in Symbol table after returning from prcedure: %d\n",next_indx);
	}
}

//Finds const in the sym table  and saves its value in the table
void putConstValue(char* name, int val){
	int i;
	for(i = 0; i < next_indx; i++){
		if(strcmp(symbolTable[i].name, name) == 0)
		{
			symbolTable[i].value = val;
			return;
		}
	}
}


//? becomessym?
int isRelation(int t){
	if(t == eqsym ||
		 t == neqsym ||
		 t == lessym ||
		 t == leqsym ||
		 t == gtrsym ||
		 t == geqsym)
		 return 1;
	return 0;
}

void emit(int op, int l, int m){
	if(cx > CODE_SIZE){
		error(28);
	}
	else{
		assembly[cx].OP = op;	//opcode
		assembly[cx].L = l;		//lexicographical level
		assembly[cx].M = m;		//modifier
		cx++;
	}
}

void term()
{
	int mulop;
	if(DEBUG_ON) printf("\nInside term\n");
	factor();
	while(tokenList[tokenIndex].number == multsym || tokenList[tokenIndex].number  == slashsym)
	{
		mulop = tokenList[tokenIndex].number;
		tokenIndex++; //move to next token
		factor();
		if(mulop == multsym){
			emit(OPR, 0, MUL);
		}
		else{
			emit(OPR, 0, DIV);
		}
	}
	if(DEBUG_ON) printf("Exiting term\n");
}

void expression()
{
	if(DEBUG_ON) printf("\nInside expression\n");
	int addop;
	if(tokenList[tokenIndex].number == plussym || tokenList[tokenIndex].number == minussym)
	{
		addop = tokenList[tokenIndex].number;
		tokenIndex++;
		term();
		if(addop == minussym)
		{
			emit(OPR, 0, NEG);
		}
	}
	else
	{
		term();
	}
	while(tokenList[tokenIndex].number == plussym || tokenList[tokenIndex].number == minussym)
	{
		addop = tokenList[tokenIndex].number;
		tokenIndex++;
		term();

		if(addop == plussym){
			emit(OPR, 0, ADD);
		}
		else
		{
			emit(OPR, 0, SUB);
		}
	}
	if(DEBUG_ON) printf("Exiting expression\n");
}

void factor()
{
	if(DEBUG_ON) printf("\nInside factor\n");
	int index;
	if (tokenList[tokenIndex].number==identsym)
		{
			tokenIndex++;
			index = lookUp(tokenList[tokenIndex].variable);
			if(symbolTable[index].kind == 2) //if it's variable
				emit(LOD, lexLevel - symbolTable[index].level, symbolTable[index].addr);
			else if(symbolTable[index].kind == 1) //if its a const
				emit(LIT, 0, symbolTable[index].value);
			else
				error(4);// const, var, proc must be followed by identifier.

			if (tokenList[tokenIndex].type==2)
				tokenIndex++;
		}
	else
		if(tokenList[tokenIndex].number==numbersym)
			{
				tokenIndex++;
				emit(LIT, 0, tokenList[tokenIndex].number);
				if (tokenList[tokenIndex].type==0)
					tokenIndex++;
		}
	else if (tokenList[tokenIndex].number==lparentsym)
	{
		tokenIndex++;
		expression();
		if (tokenList[tokenIndex].number!=rparentsym)
			error(22);  //"Error 22: Right parenthesis missing\n"
		tokenIndex++;
	}
	else
	{
		error(23); //"Error 23: The preceding factor cannot begin with this symbol\n"
		tokenIndex++;
	}
	if(DEBUG_ON) printf("Exiting factor\n");
}

void condition()
{
	if(DEBUG_ON) printf("\nInside condition\n");
	if(tokenList[tokenIndex].number == oddsym){
		emit(OPR, 0, ODD);
		tokenIndex++;
		expression();
	}
	else
	{
		expression();
		if(isRelation(tokenList[tokenIndex].number) == 0)
		{
			error(10);
		}
		int expr_code=tokenList[tokenIndex].number;
		tokenIndex++;
		expression();
		switch(expr_code)
		{
			case eqsym:
				emit(OPR, 0, EQL);
				break;
			case neqsym:
				emit(OPR, 0, NEQ);
				break;
			case lessym:
				emit(OPR, 0, LSS);
				break;
			case leqsym:
				emit(OPR, 0, LEQ);
				break;
			case gtrsym:
				emit(OPR, 0, GTR);
				break;
			case geqsym:
				emit(OPR, 0, GEQ);
				break;
		}
		if(DEBUG_ON) printf("Exiting condition\n");
	}
}

void statement()
{
	int index;
	if(DEBUG_ON) printf("\nInside statement\n");
	if (tokenList[tokenIndex].number==identsym)
	{
		tokenIndex ++;
		index = lookUp(tokenList[tokenIndex].variable);
		if(symbolTable[index].kind!= 2)
			error(30); //Variable expected
		tokenIndex++;
		
		if (tokenList[tokenIndex].number!=becomessym)
			error(13); //"Error 3: Assignment operator expected\n"
		tokenIndex++;
		expression();

		emit(STO, lexLevel - symbolTable[index].level, symbolTable[index].addr);
	}
	else if (tokenList[tokenIndex].number==callsym)
	{
		tokenIndex++;
		if (tokenList[tokenIndex].number!=identsym)
			error(4); //"Error 4: const, var, procedure must be followed by identifier\n"
		tokenIndex++;
		index = lookUp(tokenList[tokenIndex].variable);
		if(symbolTable[index].kind != 3)
			error(14);
		//emit(CAL, lexLevel - symbolTable[index].level + 1, assembly[symbolTable[index].line].M);
		emit(CAL, lexLevel - symbolTable[index].level, assembly[symbolTable[index].line].M);
		tokenIndex++;
		
	}
	else if (tokenList[tokenIndex].number==beginsym)
	{
		if(DEBUG_ON) printf("\nStatement beginsym\n");
		tokenIndex++;
		statement();
		while (tokenList[tokenIndex].number==semicolonsym)
		{
			tokenIndex++;
			statement();
		}
		//printf("After returning: This is the current token: %d, var: %s\n", tokenList[tokenIndex].number, tokenList[tokenIndex].variable);
		if (tokenList[tokenIndex].number!=endsym){
			//printf("This is the value of endsym: %d", endsym);
			error(26); //"Error 26: END expected\n"
		}
		tokenIndex++;
	}
	else if (tokenList[tokenIndex].number==ifsym)
	{
		if(DEBUG_ON) printf("\nStatement: ifsym\n");
		tokenIndex++;
		condition();
		if (tokenList[tokenIndex].number!=thensym)
			error(16); //"Error 16: then expected\n"
		else{
			tokenIndex++;
		}
		int ctemp = cx;
		int ctemp2;

		emit(JPC, 0, 0);
		statement();
		if(tokenList[tokenIndex].number == elsesym)
		{
			
			ctemp2 = cx;
			emit(JMP, 0, 0);//ctemp2 = JMP loc last line of if block
			assembly[ctemp].M = cx;
			tokenIndex++;
			statement();
			assembly[ctemp2].M = cx;
		}
		else
			assembly[ctemp].M = cx;
	}

	else if(tokenList[tokenIndex].number == readsym){
		tokenIndex++;
		if (tokenList[tokenIndex].number==2)
		{
			tokenIndex++;
			index = lookUp(tokenList[tokenIndex].variable);
			if(symbolTable[index].kind == 2)
			{
				emit(SIO, 0, READPUSH);
				emit(STO, lexLevel - symbolTable[index].level, symbolTable[index].addr);
			}
			else{
				error(12); //Error 12: Assignment to constant or procedue is not allowed.
			}
			tokenIndex++;
			if (tokenList[tokenIndex].number != semicolonsym )
			{
				int tempToken = tokenIndex;
				//printf("This is the current token: %d , var: %s\n", tokenList[tempToken].number, tokenList[tempToken].variable);
				if (tokenList[tempToken].number != endsym)
					error(10); //"Error 10: Semicolon between statements missing
			}
			tokenIndex++;
			statement();
		}
		else //when next token is not identsym
			error(29); //Error 29: Identifier expected.
		

	}
	else if(tokenList[tokenIndex].number == writesym)
	{
		tokenIndex++;
		if (tokenList[tokenIndex].number==2) //if we have an identifier
		{
			tokenIndex++;
			index = lookUp(tokenList[tokenIndex].variable);
			if(DEBUG_ON) printf("\nInside writesym var name: %s\n",tokenList[tokenIndex].variable);
			if(symbolTable[index].kind == 2) //variable
			{
				emit(LOD, lexLevel - symbolTable[index].level, symbolTable[index].addr);	//integers get their values from the stack
				emit(SIO, 0, WRITE);
			}
			else if(symbolTable[index].kind == 1) //for constant
			{
				emit(LIT, 0, symbolTable[index].value);
				emit(SIO, 0, WRITE);
			}
			else{

			}
			tokenIndex++;
			//printf("Before write: This is the current token: %d , var: %s\n", tokenList[tokenIndex].number, tokenList[tokenIndex].variable);
			if (tokenList[tokenIndex].number != semicolonsym )
			{
				int tempToken = tokenIndex;
				//printf("This is the current token: %d , var: %s\n", tokenList[tempToken].number, tokenList[tempToken].variable);
				if (tokenList[tempToken].number != endsym)
					error(10); //"Error 10: Semicolon between statements missing
			}
			//printf("After write: This is the current token: %d , var: %s\n", tokenList[tokenIndex].number, tokenList[tokenIndex].variable);
			tokenIndex++;
			statement();
		}
		else //when next token is not identsym
			error(29); //Error 29: Identifier expected.
		///CHECK FOR SEMICOLON
	}

	else if (tokenList[tokenIndex].number==whilesym)
	{
		int cx1 = cx;
		tokenIndex++;
		condition();
		int cx2 = cx;
		emit(JPC, 0, 0);
		if (tokenList[tokenIndex].number!=dosym)
			error(18); //"Error 18: do expected\n"
		else{
			tokenIndex++;
		}
		statement();
		emit(JMP, 0, cx1);
		assembly[cx2].M = cx;
	}
	if(DEBUG_ON) printf("Exiting statement\n");
}

void block()
{
	if(DEBUG_ON) printf("\nInside block\n");
	num_vars=0;
	int cx_block = cx;
	int keepOwnIndex;
	emit(JMP, 0, 0);
	if (tokenList[tokenIndex].number==constsym)
	{
		do
		{
			tokenIndex++; //move to the next token
			if (tokenList[tokenIndex].number!=identsym)
				error(4); //"Error 4: const, var, procedure must be followed by identifier\n"
			tokenIndex++;
			addSymbol(tokenList[tokenIndex].variable,1,lexLevel, -7);
			sprintf(const_name, "%s", tokenList[tokenIndex].variable);
			tokenIndex++;
			if (tokenList[tokenIndex].number!=eqsym)
				error(3); //"Error 3: Identifier must be followed by ="
			tokenIndex++;
			if (tokenList[tokenIndex].number!=numbersym)
				error(2); //"Error 2: = must be followed by a number\n"
			tokenIndex++;
			putConstValue(const_name,tokenList[tokenIndex].number);
			tokenIndex++;
		}
		while (tokenList[tokenIndex].number==commasym);
		//tokenIndex++;
		if (tokenList[tokenIndex].number!=semicolonsym)
			error(10); //"Error 10: Semicolon between statements missing\n"
		tokenIndex++;
	}
	if (tokenList[tokenIndex].number==varsym)
	{
		do
		{
			tokenIndex++; //move to the next token
			if (tokenList[tokenIndex].number!=identsym)
				error(4); //"Error 4: const, var, procedure must be followed by identifier\n"
			tokenIndex++;
			addSymbol(tokenList[tokenIndex].variable, 2, lexLevel, -7);
			if(tokenList[tokenIndex].type == 2){
				tokenIndex++;
			}
		}
		while (tokenList[tokenIndex].number==commasym);
		if (tokenList[tokenIndex].number!=semicolonsym)
			error(10); //"Error 10: Semicolon between statements missing\n"
		tokenIndex++;
		//emit(INC, 0, num_vars+4);

	}
	while (tokenList[tokenIndex].number==procsym)
	{

		int procIndex=-1;
		tokenIndex++; //move to the next token
		if (tokenList[tokenIndex].number!=identsym)
			error(4); //"Error 4: const, var, procedure must be followed by identifier\n"
		tokenIndex++;
		addSymbol(tokenList[tokenIndex].variable, 3, lexLevel, cx);
		
		procIndex = lookUp(tokenList[tokenIndex].variable);
		if(DEBUG_ON) printf("\n%s procIndex: %d \n", tokenList[tokenIndex].variable, procIndex);
		tokenIndex++;
		if (tokenList[tokenIndex].number!=semicolonsym)
			error(17); //"Error 17: Semicolon or } expected\n"
		tokenIndex++;
		lexLevel++;
		block();
		emit(OPR, 0, 0);
		lexLevel--;
		deleteSymbols(procIndex);
		tokenIndex++;
	}
//	indexTemp = keepOwnIndex;
	assembly[cx_block].M = cx; /*set jmp to correct line */
	//symbolTable[indexTemp].codeAddr = cx + 1;
	emit(INC, 0, getNumVars(lexLevel)+4);

	statement();

	if(DEBUG_ON) printf("Exiting block\n");

}

void program()
{
	if(DEBUG_ON) printf("\nInside program\n");
	block();
	if (tokenList[tokenIndex].number!=periodsym)
		error(9); //"Error 9: Period expected\n"

	//we need to halt at the end of the program
	emit(SIO, 0, 2);
	if(DEBUG_ON) printf("Exiting program\n");
}

// int isDigit(char ch)
// {
    // int ascii_val=(int)ch;
    // if (ascii_val>47 && ascii_val<58){
        // return 1;
	// }
    // else{
        // return 0;
	// }
// }

int isAlpha(char* ch)
{
    //check for letter
	if (((int)ch[0]>64 && (int)ch[0]<91) || ((int)ch[0]>96 && (int)ch[0]<123)){
		return 1;
	}
	else{
		return 0;
	}
}

void readTokens(FILE *input){
	char* temp = malloc(sizeof(char)*12);
	//lexeme tokenList[CODE_SIZE];
	lexeme *token = tokenList;
	if(!feof(input))
	{
	    //int i=0;
		int ident = 0;
		fscanf(input, "%s", temp);
		//printf("%s\n", temp);
		if(isAlpha(temp)){
			//printf("Name: %s\n", temp);
			strcpy(token[tokenList_size].variable, temp);
			token[tokenList_size].number = -1;
			token[tokenList_size].type = 2;
			tokenList_size++;
			return;
		}
		sscanf(temp, "%d", &ident);
		token[tokenList_size].type = 0;
		token[tokenList_size].number = ident;
		strcpy(token[tokenList_size].variable, "\0");
		//printf("Identifier: %d\n", ident);
		tokenList_size++;
		return;
	}
	return;
	//return null;
}

int size(lexeme tokens[]){
	int counter=0, i=0;
	while(strcmp(tokens[i].variable, "END OF TOKENS") != 0){
		i++;
		counter++;
	}
	return counter;
}

int fileSize(){
	FILE *input;

	input = fopen(lList, "r");
	char c; //= ' ';
	int charCounter = 0;
	while(!feof(input)){
		fscanf(input, "%c", &c);
		charCounter++;
	}
	return charCounter;
}

void parse(lexeme tokens[], int numTokens){
	//int numTokens = fileSize();
	//lexeme tokens[numTokens];
	strcpy(tokens[numTokens].variable, "END OF TOKENS");
	if(DEBUG_ON) printf("\nThis is the size of tokens: %d\n", size(tokens));
	FILE *input;
	input = fopen(lList, "r");
	int i=0;
	while(!feof(input))/*  && i < size(tokens) ) */
	{
		readTokens(input);
		//printf("Type: %d, Number: %d, Variable: %s\n", tokens[i].type, tokens[i].number, tokens[i].variable);
		i++;
	}
	fclose(input);
	if(DEBUG_ON) printf("\n**********************\n");
	for(i=0; i<tokenList_size; i++)
	{
		if(DEBUG_ON) printf("%d: Type: %d, Number: %d, Variable: %s\n", i,tokenList[i].type, tokenList[i].number, tokenList[i].variable);
	}
}

// void main()
// {
	// int numTokens = fileSize();
	// lexeme tokens[numTokens];
	// parse(tokens, numTokens);
	// //VERY IMPORTANT TO INIT SYM TABLE
	// initSymbolTable();
	// program();
	// int i;
	// for (i=0; i<next_indx; i++)
	// {
		// if(DEBUG_ON) printf("\nName: %s, addr: %d, value: %d, kind: %d\n", symbolTable[i].name, symbolTable[i].addr,symbolTable[i].value, symbolTable[i].kind);
	// }

	// translateOPAssembly(assembly, cx);
	// if(DEBUG_ON) printf("\n**********************\n");
	// for (i=0; i<cx; i++)
	// {
		// if(DEBUG_ON) printf("\nOP: %d, L: %d, M: %d\n", assembly[i].OP, assembly[i].L, assembly[i].M);
	// }
//}

void translateOPAssembly(instruction *inst_arr, int arr_size){
	int i = 0;
	//printf("Line \tOP \tL \tM\n");
	OP_Strings = malloc(MAX_CODE_LENGTH * sizeof(char*));
	while(i < arr_size){
		OP_Strings[i] = malloc(3 * sizeof(char));
		int OpCode = inst_arr[i].OP;
		char* transOP = malloc(sizeof(char)*10);
		int M = inst_arr[i].M;
		int L = inst_arr[i].L;
		switch(OpCode){
			case 0:
				strcpy(transOP, "EMTPY");
				break;
			case 1:
				strcpy(transOP, "LIT");
				break;
			case 2:
				strcpy(transOP, "OPR");
				break;
			case 3:
				strcpy(transOP, "LOD");
				break;
			case 4:
				strcpy(transOP, "STO");
				break;
			case 5:
				strcpy(transOP, "CAL");
				break;
			case 6:
				strcpy(transOP, "INC");
				break;
			case 7:
				strcpy(transOP, "JMP");
				break;
			case 8:
				strcpy(transOP, "JPC");
				break;
			case 9:
				strcpy(transOP, "SIO");
				break;
			default:
				break;
		}
		if(OpCode != 0){
			//printf("%2d \t%s \t%d \t%d", i, transOP, L, M);
		}
		strcpy(OP_Strings[i], transOP);

		//printf("\n");
		i++;
	}
}

int parserMain(int aArg){
	//printf("Parser is running\n");
	int numTokens = fileSize();
	lexeme tokens[numTokens];
	parse(tokens, numTokens);
	//VERY IMPORTANT TO INIT SYM TABLE
	initSymbolTable();
	program();

	translateOPAssembly(assembly, cx);
	if(aArg == 1)
			if(DEBUG_ON) printf("\n**********************\n");
	FILE *inputOutput;
	char* output = "mcode.txt";
	inputOutput = fopen(output, "w");
	int i;
	for (i=0; i<cx; i++)
	{
		if(assembly[i].OP == 9 && assembly[i].L == 0 && assembly[i].M == 2){
			fprintf(inputOutput, "%d %d %d", assembly[i].OP, assembly[i].L, assembly[i].M);
			if(aArg == 1){
				printf("%d %d %d", assembly[i].OP, assembly[i].L, assembly[i].M);
			}
		}
		else{
			fprintf(inputOutput, "%d %d %d\n", assembly[i].OP, assembly[i].L, assembly[i].M);
			if(aArg == 1){
				printf("%d %d %d\n", assembly[i].OP, assembly[i].L, assembly[i].M);
			}
		}
		//if(DEBUG_ON) printf("%d %d %d\n", assembly[i].OP, assembly[i].L, assembly[i].M);
	}
	fclose(inputOutput);
	if (DEBUG_ON) printf("\nCX: %d\nClosed mcode", cx);
	return errorBool;
}

// void main(){
	// int aArg = 1;
	// parserMain(aArg);
// }
