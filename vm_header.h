#ifndef VM_HEADER_H_INCLUDED
#define VM_HEADER_H_INCLUDED

#define MAX_STACK_HEIGHT  2000
#define MAX_CODE_LENGTH  500
#define MAX_LEXI_LEVELS  3

enum OPR_Type{RET = 0, NEG = 1, ADD = 2, SUB = 3, MUL = 4, DIV = 5, ODD = 6, MOD = 7, EQL = 8 , NEQ = 9, LSS = 10, LEQ = 11, GTR = 12, GEQ = 13};
enum SIO_Type{WRITE = 0, READPUSH = 1, HALT = 2};
enum ISA_Type{EMPTY, LIT, OPR, LOD, STO, CAL, INC, JMP, JPC, SIO};
typedef enum OPR_Type OPR1;
typedef enum SIO_Type SIO1;
typedef enum ISA_Type ISA;

typedef struct instruction{
	int OP;
	int L;
	int M;
}instruction;

int base(int level, int b);
void Fetch();
void switchOnOPR(int M);
void switchOnSIO(int M);
void switchOnISA(instruction lineIn);
void Execute();
void translateOP(FILE* fp);
void translateOPAssembly(instruction *inst_arr, int arr_size);
void readInputFile(FILE* fp);
void printStack(FILE* fp);
void vmMain(int vArg);

#endif
