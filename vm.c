#include <stdio.h>
#include <stdlib.h>
//#include <math.h>
#include <string.h>
#include "vm_header.h"
// #define MAX_STACK_HEIGHT  2000
// #define MAX_CODE_LENGTH  500
// #define MAX_LEXI_LEVELS  3
#define VM_DBG 0
int sp = 0;
int bp = 1;
int pc = 0;
int oldPC = 0;
int codeSize = 0;
int halt = 0;
int vArg = 0;
int bp_record[5], bp_index=1;
// enum OPR_Type{RET = 0, NEG = 1, ADD = 2, SUB = 3, MUL = 4, DIV = 5, ODD = 6, MOD = 7, EQL = 8 , NEQ = 9, LSS = 10, LEQ = 11, GTR = 12, GEQ = 13};
// enum SIO_Type{WRITE = 0, READPUSH = 1, HALT = 2};
// enum ISA_Type{EMPTY, LIT, OPR, LOD, STO, CAL, INC, JMP, JPC, SIO};
// typedef enum OPR_Type OPR1;
// typedef enum SIO_Type SIO1;
// typedef enum ISA_Type ISA;
int stack[MAX_STACK_HEIGHT];
char* file = "mcode.txt";
char* output = "stacktrace.txt";
// typedef struct instruction{
	// int OP;
	// int L;
	// int M;
// }instruction;
instruction ir;
instruction code[MAX_CODE_LENGTH];
char** OP_Strings;
int base(int level, int b) {
	while (level > 0){
		b = stack[b + 1]; // NOTE THE CHANGE! We now have + 2 !
		level--;
	}
	return b;
}
void Fetch(){
	if (VM_DBG) printf("\nInside fetch\n");
	ir = code[pc];
	oldPC = pc;
	pc++;
	// if (pc>codeSize)
		// exit;
}
void switchOnOPR(int M){
	switch(M){
		case RET:
			{
				sp = bp - 1;
				pc = stack[sp + 4];
				bp = stack[sp + 3];
				/*adjust bp value in bp_record array to remove AR separator
				 for printState function */
				bp_index--;
				bp_record[bp_index]=0;
			}
			break;
		case NEG:
			{
				stack[sp] = -stack[sp];
			}
			break;
		case ADD:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] + stack[sp + 1];
			}
			break;
		case SUB:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] - stack[sp + 1];
			}
			break;
		case MUL:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] * stack[sp + 1];
			}
			break;
		case DIV:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] / stack[sp + 1];
			}
			break;
		case ODD:
			{
				stack[sp] = stack[sp]%2;
			}
			break;
		case MOD:
			{
				sp = sp - 1;
				stack[sp] = stack[sp]%stack[sp + 1];
			}
			break;
		case EQL:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] == stack[sp + 1];
			}
			break;
		case NEQ:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] != stack[sp + 1];
			}
			break;
		case LSS:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] < stack[sp + 1];
			}
			break;
		case LEQ:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] <= stack[sp + 1];
			}
			break;
		case GTR:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] > stack[sp + 1];
			}
			break;
		case GEQ:
			{
				sp = sp - 1;
				stack[sp] = stack[sp] >= stack[sp + 1];
			}
			break;
		// default:
			// break;
	}
	return;
}
void switchOnSIO(int M){
	switch(M){
		case WRITE:
			{
				printf("\noutput to screen: %d\n", stack[sp]);
				sp = sp - 1;
			}
			break;
		case READPUSH:
			{
				sp = sp + 1;
				printf("\nPlease enter a value to be inserted into the stack: \n");
				scanf("%d", &stack[sp]);
			}
			break;
		case HALT:
			{

				halt = 1;
			}
			break;
		// default:
			// break;
	}
	return;
}
void switchOnISA(instruction lineIn){
		int M = lineIn.M;
		int L = lineIn.L;
		int OP = lineIn.OP;

		switch(OP){
			case EMPTY:
				break;
			case LIT:
				{
					sp = sp + 1;
					stack[sp] = M;
				}
				break;
			case OPR:
				{
					switchOnOPR(M);
				}
				break;
			case LOD:
				{
					sp = sp + 1;
					stack[sp] = stack[base(L, bp)+ M];
				}
				break;
			case STO:
				{
					stack[base(L, bp) + M] = stack[sp];
					sp = sp - 1;
				}
				break;
			case CAL:
				{
					stack[sp + 1] = 0;  //return value (FV)
					stack[sp + 2] = base(L, bp);  // static link (SL)
					stack[sp + 3] = bp;     // dynamic link (DL)
					stack[sp + 4] = pc;      // return address (RA)
					bp=sp+1;
					bp_record[bp_index]=bp;
					bp_index++;
					pc = M;
				}
				break;
			case INC:
				{
						sp = sp + M;
						 if (VM_DBG) printf("INC");
				}
				break;
			case JMP:
				{
					pc = M;
				}
				break;
			case JPC:
				{
					if ( stack[ sp ] == 0 )
					{
						pc = M;
					}
					sp = sp - 1;
				}
				break;
			case SIO:
				{
					switchOnSIO(M);
				}
				break;
			// default:
				// break;
		}
}
void Execute(){
	if (VM_DBG) printf("\nInside execute\n");
	switchOnISA(ir);
}
void translateOP(FILE* fp){

	int i = 0;
	fprintf(fp, "Line \tOP \tL \tM\n");
	OP_Strings = malloc(MAX_CODE_LENGTH * sizeof(char*));
	while(i < codeSize){
		OP_Strings[i] = malloc(3 * sizeof(char));
		int OpCode = code[i].OP;
		char* transOP = malloc(sizeof(char)*10);
		int M = code[i].M;
		int L = code[i].L;
		switch(OpCode){
			case 0:
				strcpy(transOP, "EMTPY");
				break;
			case 1:
				strcpy(transOP, "LIT");
				break;
			case 2:
				strcpy(transOP, "OPR");
				break;
			case 3:
				strcpy(transOP, "LOD");
				break;
			case 4:
				strcpy(transOP, "STO");
				break;
			case 5:
				strcpy(transOP, "CAL");
				break;
			case 6:
				strcpy(transOP, "INC");
				break;
			case 7:
				strcpy(transOP, "JMP");
				break;
			case 8:
				strcpy(transOP, "JPC");
				break;
			case 9:
				strcpy(transOP, "SIO");
				break;
			default:
				break;
		}
		if(OpCode != 0){
			fprintf(fp, "%2d \t%s \t%d \t%d", i, transOP, L, M);
			if(vArg == 1){
				printf("\n%2d \t%s \t%d \t%d", i, transOP, L, M);
			}
		}
		strcpy(OP_Strings[i], transOP);

		fprintf(fp, "\n");
		i++;
	}
}

void readInputFile(FILE* input){

	int i = 0;
	//fp = fopen(file, "r");
	// FILE *mcode_file;
	// mcode_file = fopen("mcode.txt", "r");
	if (VM_DBG) printf("\nOpened mcode\n");
	while(!feof(input))
	{
		fscanf(input, "%d %d %d", &code[i].OP, &code[i].L, &code[i].M);
		if (VM_DBG) printf("This is what is in code at index %d: %d %d %d \n", i, code[i].OP, code[i].L, code[i].M);
		i++;
	}
	codeSize = i;
	fclose(input);
	//fclose(fp);
}
void printStack(FILE* fp)
{
	int i = 1, tempSP = sp, counter=1, k;
	while(counter <= sp && sp > 0)
	{
		/*if(i == bp && bp != 1)
		{
			fprintf(fp, "| ");
			if(vArg == 1)
				printf("| ");
		}*/
		//print bar "|" to separate ARs when necessary
		for (k=1; k<=bp_index; k++)
		{
			if( (counter!=0)&&(counter==bp_record[k]) )
			{
				fprintf(fp,"| ");
				if(vArg == 1)
					printf("| ");
		
			}
		}
				    
		fprintf(fp, "%d ", stack[counter]);
		if(vArg == 1)
			printf("%d ", stack[counter]);
			
		counter++;
	}
}
void vmMain(int Arg){
	vArg = Arg;
	//printf("VM is running\n");
	int i = 0, k;
	 /*store current bp in an array,
	     this array will be used later to separate ARs in the output file with "|"*/
	bp_record[0]=bp;
	for (k=1; k<5; k++)
		bp_record[k]=0;
	bp_index=1;
	FILE *fp, *input;
	input = fopen("mcode.txt", "r");
	readInputFile(input);
	fp = fopen(output, "w");
	translateOP(fp);
	fprintf(fp, "\n\t\t\t\tL\tM\tpc\tbp\tsp\tstack\n");
	fprintf(fp, "Initial Values\t\t\t\t\t%d\t%d\t%d", pc, bp, sp);
	if(vArg == 1){
		printf("\n\t\t\t\tL\tM\tpc\tbp\tsp\tstack\n");
		printf("Initial Values\t\t\t\t\t%d\t%d\t%d", pc, bp, sp);
	}
	while(halt != 1)
	{

		Fetch();
		//printStack(fp);
		Execute();
		if  (VM_DBG) printf("\n%d\t%s\t\t%d\t%d\t%d\t%d\t%d\t", oldPC, OP_Strings[oldPC], ir.L, ir.M, pc, bp, sp);
		fprintf(fp, "\n\t%d\t%s\t\t%d\t%d\t%d\t%d\t%d\t", oldPC, OP_Strings[oldPC], ir.L, ir.M, pc, bp, sp);
		if(vArg == 1){
			printf("\n\t%d\t%s\t\t%d\t%d\t%d\t%d\t%d\t", oldPC, OP_Strings[oldPC], ir.L, ir.M, pc, bp, sp);
		}
		printStack(fp);
		//printStack(fp);

	}
	fclose(fp);
	//printf("Finished!\n");
}

// void main(){
	// vmMain(1);
// }
